FROM httpd:alpine
LABEL Author = "TIM"
EXPOSE 80
COPY . /usr/local/apache2/htdocs
COPY ./index-edge.html /usr/local/apache2/htdocs/index.html
RUN chmod 644 /usr/local/apache2/htdocs/index.html
RUN chmod 644 /usr/local/apache2/htdocs/TIM_Logo_2016.png
RUN chmod 644 /usr/local/apache2/htdocs/TIM_Video.mp4
RUN chmod 644 /usr/local/apache2/htdocs/world.jpg
RUN chmod 644 /usr/local/apache2/htdocs/style.css